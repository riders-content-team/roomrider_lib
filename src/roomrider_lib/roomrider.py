#!/usr/bin/env python
from robot_control import RobotControl
import rospy
from std_srvs.srv import Trigger, TriggerRequest
import math
from std_msgs.msg import String
import time
from angles import normalize_angle
import os
import requests

class Robot:
    def __init__(self, robot_name):

        rospy.init_node("roomrider_controller")
        self.robot_pose_pub = rospy.Publisher("/robot_pose/", String, queue_size=10)
        
        self.check_right = False
        self.right_calls = 0
        self.check_forward = False
        self.forward_calls = 0

        self.out_of_bounds = False

        #Roomrider
        robot_data = {
        "robot_name": robot_name,
        "base_link": "link",
        }

        pos_data = {
        "x": (-1.5),
        "y": (-1.5),
        "z": (0),
        "yaw": (0)
        }

        event_data = {
        "unit_length": (1),
        "event_period": (2)
        }

        self.robot_name = robot_name
        self.pos_x = pos_data['x']
        self.pos_y = pos_data['y']
        self.dir = pos_data['yaw']

        self.init_game_controller()

        if not self.status == "no_robot":
            self.robot = RobotControl(robot_data, pos_data, event_data)

    def init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)

            time.sleep(0.1)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()
            self.status = resp.message

            if not self.status == "no_robot":
                self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
                self.robot_pose_pub.publish(self.robot_pose)

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"

            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            rospy.logerr("Service call failed: %s" % (e,))

    def check_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 5.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()

            self.status = resp.message
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))

    def move_forward(self):
        if self.check_forward:
            self.achieve(48)
            self.forward_calls += 1
            if self.forward_calls >= 3:
                self.achieve(49)

        if abs(normalize_angle(self.dir - math.pi/2)) < 0.01 and self.pos_x <= -1.5:
            self.out_of_bounds = True
            return
        elif abs(normalize_angle(self.dir)) < 0.01 and self.pos_y >= 1.5:
            self.out_of_bounds = True
            return
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01 and self.pos_y <= -1.5:
            self.out_of_bounds = True
            return
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01 and self.pos_x >= 1.5:
            self.out_of_bounds = True
            return
        
        if not self.status == "free_roam" and not self.status == "no_robot":
            self.check_game_controller()

        if not self.status == "stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.move_distance(1)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

    def move_backward(self):

        if abs(normalize_angle(self.dir - math.pi/2)) < 0.01 and self.pos_x >= 1.5:
            self.out_of_bounds = True
            return
        elif abs(normalize_angle(self.dir)) < 0.01 and self.pos_y <= -1.5:
            self.out_of_bounds = True
            return
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01 and self.pos_y >= 1.5:
            self.out_of_bounds = True
            return
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01 and self.pos_x <= -1.5:
            self.out_of_bounds = True
            return

        if not self.status == "free_roam" and not self.status == "no_robot":
            self.check_game_controller()

        if not self.status == "stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.move_distance(-1)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

    def turn_left(self):
        if self.out_of_bounds:
            return

        if not self.status == "free_roam" and not self.status == "no_robot":
            self.check_game_controller()

        if not self.status == "stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(math.pi/2)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

    def turn_right(self):
        if self.out_of_bounds:
            return

        if self.check_right:
            self.achieve(46)
            self.right_calls += 1
            if self.right_calls >= 3:
                self.achieve(47)
        
        if self.forward_calls == 3 and self.check_forward:
            self.achieve(50)

        if not self.status == "free_roam" and not self.status == "no_robot":
            self.check_game_controller()

        if not self.status == "stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(-math.pi/2)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

    def is_ok(self):
        self.achieve(44)

        self.check_right = True
        self.right_calls = 0
        self.check_forward = True
        self.forward_calls = 0

        if self.out_of_bounds:
            print("DirtRider: The path is blocked")
            return False

        if self.status == "stop" or self.status == "no_robot":
            return False
        return True
